import { Router } from "express";
import React from "react";
import ReactDOM from "react-dom/server";
import createDocument from "./create-document";
import * as Api from "./api";
import RepositoryList from "./components/repository-list";

const router = Router();
export default router;

router.get("*", async (req, res) => {
  const repositories = await Api.fetchRepositories("react");

  const html = ReactDOM.renderToString(
    <RepositoryList repositories={repositories} />
  );

  res.send(
    createDocument({
      title: "My First React Page",
      html
    })
  );
});
