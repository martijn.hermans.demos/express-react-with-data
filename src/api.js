import Axios from "axios";

const axios = Axios.create({
  baseURL: "https://api.github.com/"
});

export async function fetchRepositories(query, sort = "created", page = 1) {
  try {
    const { data } = await axios.get("/search/repositories", {
      params: {
        q: query,
        sort,
        page
      }
    });
    return data.items;
  } catch (e) {
    console.log(e);
  }
  return [];
}
